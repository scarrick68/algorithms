function newWinner(participant){
	var winner = Math.floor(Math.random() * participant) + 1;
	if(winner === participant){
		return true
	}
	return false
}

function runRaffle(numberParticipcants){
	var winner = 1;
	for(var i = 1; i <= numberParticipcants; i++){
		var changeWinner = newWinner(i)
		if(changeWinner){
			winner = i;
		}
	}
	return winner
}

function testDistribution(numberParticipcants){
	var results = initArr(numberParticipcants)
	for(var i = 0; i < 1000000; i++){
		var winner = runRaffle(numberParticipcants);
		results[winner-1] = results[winner-1]+1;
	}
	console.log(results)
}

function initArr(numberParticipcants){
	var arr = []
	for(var i = 0; i < numberParticipcants; i++){
		arr[i] = 0;
	}
	return arr
}

testDistribution(5)