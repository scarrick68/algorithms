/*
	Problem:
	- Create a linked list out of the nodes at each depth
	of the tree. There will be D linked lists for a tree of
	depth D.

	Approach:
	- Modify any tree traversal to keep track of depth and add
	appropriate nodes to appropriate lists.
	- Modify a breadth-first search to know when it's reached the
	end of a level in the tree, create a new linked list and
	start adding items again.
*/

// btree level to linked list
// using parameter tree because code is generalizable to
// n-ary trees. Would need to take specified root node
// to work for arbitrary graph.
// depth variable isn't totally necessary, but I think it's
// better semantically than matrix.length-1
function levelToLL(tree){
	var queue = []
	var matrix = [[]]
	var depth = 0
	var thisLevel = 0
	var nextLevel = 0
	queue.push(tree.root)
	thisLevel++
	while(queue.length > 0){
		var node = queue.unshift()
		thisLevel--
		matrix[depth].push(node)
		for(var node in node.adjacent){
			queue.push(node)
			nextLevel++
		}
		if(thisLevel === 0){
			matrix.push([])
			depth++
			thisLevel = nextLevel
			nextLevel = 0
		}
	}
}