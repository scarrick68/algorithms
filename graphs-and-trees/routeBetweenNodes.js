/*
	** Code at bottom **
	
	Problem:
	- Find out whether there is a route between two nodes in
	directed graph. 

	Assumptions:
	- Graph has cycles.
	- Graph is unweighted.
	- If graph does not have cycles and we can take advantage
	of some property of the graph it becomes much easier. Like
	a binary tree.

	Approach:
	- Basically we just do a breadth first search for an
	arbitrary graph. Should be O(N) time complexity as long
	as we don't visit nodes in a cycle twice.
	- If graph has some organization, like btree, then
	depth first will be more efficient.
*/

/*
	Tests:
	- a and b are in different graphs. This test can fail because
	node id is just based on size. Could be more robust by doing
	deep equality between current node and b.
	- a is the only node
	- a is null
	- b doesn't exist
	- a and b exist, and there is a route
	- does order matter? if there is a route b to a but not the
	other way, since the graph is directed, does that still count
	too?

	These tests have not been written and run yet.
*/

// Create graph data structure
// Make routeBetweenNodes function and put it on proto chain
// That way any graph instance can determine route between
// any two of it's nodes

// We need to access it through some root although a root
// is not strictly necessary in graph theory
function Graph(value){
	this.root = new Node(value)
	this.size = 1
}

// Node objects for the graph
function Node(value, size){
	this.id = size + 1
	this.val = value
	this.adjacent = {}
}

Graph.prototype.routeBetween = function(a, b) {
	var queue = []
	// visit and push children onto queue
	// remove Node from queue, visit, push children
	console.log(a.value)
	for(var node in a.adjacent){
		queue.push(node)
	}

	while(queue.length !== 0){
		var node = queue.unshift()
		console.log(node)
		if(node.id === b.id){
			console.log('found route')
			return true
		}
	}

	return false
};