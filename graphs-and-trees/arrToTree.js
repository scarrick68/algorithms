/*
	Problem:
	- Create a binary tree of minimal height from a sorted array.

	Approach:
	- Find middle element, make it the root node,
	- Recursively construct left and right subtrees
	- Find the middle of the subarrays, make those roots of subtrees
*/

function Node(val){
	this.val = val
	this.left = null
	this.right = null
}

function arrToTree(arr, start, end){
	if(start > end) return
	var mid = Math.floor((start+end)/2)
	var node = new Node(arr[mid])

	// Build left and right subtrees recursively
	node.left = arrToTree(arr, start, mid-1)
	node.right = arrToTree(arr, mid+1, end)

	// Ultimately returns tree root
	return node
}

var arr = [1,2,3,4,5,6,7]
var root = arrToTree(arr, 0, arr.length-1)
console.log(root)