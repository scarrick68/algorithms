function permute(str){
	doPermute(str, '')
}

function doPermute(str, pre){
	if(str.length === 0){
		console.log(pre)
	}
	else{
		for(var i = 0; i < str.length; i++){
			/* 	This line trims the character from position i in
				str, catting whatever is before and after.
				*/
			var rem = str.substring(0, i) + str.substring(i+1);
			/*
				pre + str.charAt(i) ensures that a prefix starting
				with each letter is built. when i === 0 it will build
				a permutation starting with h. when i === 1 it will
				do the same with a. rem represents the rest of the
				string minus the characters in the permutation
			*/
			doPermute(rem, pre + str.charAt(i))
		}
	}
}

var str = 'ham'
console.log('testing charAt', str.charAt(0))
console.log('testing substring', str.substring(0, 0))
permute(str)