// Cracking The Coding Interview
// Recursion and Dynamic Programming
// 8.3

// Not posting problem description for copyright concerns.
// Please see Cracking The Coding Interview, 6th edition
// for full problem description.

// Naive implementation just iterating over the array...
// but with recursion instead of a loop
function magicIndex(arr, index){
	index = index || 0;
	if(arr[index] === index){
		return index
	}
	if(index === arr.length -1 && index !== arr[index]){
		return false
	}

	index = magicIndex(arr, index+1)
	return index
}

// modified binary search reducing time complexity to log(N)
function binMagicIndex(arr, index, beg, end){
	// no need for beg an end in the statement. They'll
	// all be initialized if index is initialized.
	if(index === undefined){
		index = index || Math.floor(arr.length/2);
		beg = beg || 0;
		end = end || arr.length - 1;
	}

	if(arr[index] === index){
		return index
	}
	else if(beg - end === 0 && arr[index] !== index){
		return false
	}

	// Check left half
	if(arr[index] > index){
		end = index;
		index = Math.floor((beg + end) / 2);
		index = binMagicIndex(arr, index, beg, end);
	}
	// Check right half
	else if(arr[index] < index){
		beg = index;
		index = Math.ceil((beg + end) / 2);
		index = binMagicIndex(arr, index, beg, end);
	}

	return index
}

var arr = [0,1,2,3,4]
var index = magicIndex(arr)
console.log(index)

arr = [-1,1,2,3,4]
index = magicIndex(arr)
console.log(index)

arr = [-1,-1,2,3,4]
index = magicIndex(arr)
console.log(index)

arr = [0,1,2,3,4]
var index = binMagicIndex(arr)
console.log(index)

arr = [-1,1,3,3,4]
index = binMagicIndex(arr)
console.log(index)

arr = [-1,-1,1,3,4]
index = binMagicIndex(arr)
console.log(index)

// all values below their index
arr = [-1,-1,-1,-3,-4]
index = binMagicIndex(arr)
console.log(index)

// all values above their index
arr = [10,10,10,10,10]
index = binMagicIndex(arr)
console.log(index)