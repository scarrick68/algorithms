// Cracking The Coding Interview
// Recursion and Dynamic Programming
// 8.2

// Not posting problem description for copyright concerns.
// Please see Cracking The Coding Interview, 6th edition
// for full problem description.

// Representing valid spots in grid as 1. Invalid spots as 0.

//  Invalid paths that were followed will be shown up to where
//  the robot got stuck, but it will not show the robot backtracking
//  to find another valid move. It will just go back to the last
//  point it had another valid move and then continue.
//  See example 4: (0,0), (1,0), gets stuck and returns up call
//  stack to (0,0) which is not printed again. Then continues to
//  (0,1) which is shown.

// wrapper function to init values
function robotGrid(grid){
	var posX = 0
	var posY = 0
	var path = {found: false, path: [{x: 0, y: 0}]};
	path = findPath(grid, path, posX, posY);
	return path
}

// Lot of if statements. Maybe can make this cleaner.
function findPath(grid, path, posX, posY){
	// base case. robot has found a path.
	if(posY === grid.length - 1 && posX === grid[grid.length - 1].length - 1){
		// path.path.push({x: posX, y: posY})
		path.found = true;
		return path
	}

	var move = validMoves(grid, posX, posY)

	// go right
	if(move.inHorizBound && move.rightValid){
		path.path.push({x: posX+1, y: posY})
		findPath(grid, path, posX+1, posY)
	}
	// go down unless path has already been found
	if(move.inVertBound && move.downValid && !path.found){
		path.path.push({x: posX, y: posY+1})
		findPath(grid, path, posX, posY+1)
	}

	// no options, return up call stack and try again.
	return path
}

// check if right and down moves are in grid bounds
function validMoves(grid, posX, posY){
	var inHorizBound = (posX+1) <= grid[posY].length - 1
	var inVertBound = (posY+1) <= grid.length - 1
	
	var rightValid = false;
	var downValid = false;

	if(inHorizBound){
		rightValid = grid[posY][posX+1] !== 0;
	}
	if(inVertBound){
		downValid = grid[posY+1][posX] !== 0;
	}

	return {inHorizBound: inHorizBound,
			inVertBound: inVertBound,
			rightValid: rightValid,
			downValid: downValid}
}

var grid = [[1,1,1],
			[1,1,1],
			[1,1,1]];

var path = robotGrid(grid);
console.log('\ngrid1\n')
console.log(path)

grid = [[1,0,1],
		[1,0,1],
		[1,1,1]];

path = robotGrid(grid);
console.log('\ngrid2\n')
console.log(path)

grid = [[1,0,1],
		[1,1,1],
		[1,0,1]];

path = robotGrid(grid);
console.log('\ngrid3\n')
console.log(path)

grid = [[1,1,0],
		[1,0,1],
		[1,1,1]];

path = robotGrid(grid);
console.log('\ngrid4\n')
console.log(path)