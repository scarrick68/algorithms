// Cracking The Coding Interview
// Recursion and Dynamic Programming
// 8.6

// Not posting problem description for copyright concerns.
// Please see Cracking The Coding Interview, 6th edition
// for full problem description.

// Towers of Hanoi
function towers(rings){
	var tower1 = new Tower(0)
	var tower2 = new Tower(1)
	var tower3 = new Tower(2)

	// put the disks on the first tower
	for(var i = rings-1; i >= 0; i--){
		tower1.add(i)
	}

	tower1.moveDisks(rings, tower3, tower2, tower1, tower2, tower3)
	console.log(tower1, tower2, tower3)
}

function Tower(index){
	this.stack = []
	this.index = index
}

Tower.prototype.add = function(disk) {
	var lastIndex = this.stack.length-1
	if(this.stack.length === this.stack[lastIndex] <= disk){
		console.error('invalid disk placement')
	}
	else{
		this.stack.push(disk)
	}
};

Tower.prototype.moveDisks = function(N, destination, buffer, tower1, tower2, tower3) {
	if(N > 0){
		this.moveDisks(N-1, buffer, destination, tower1, tower2, tower3)
		this.moveTopTo(destination)
		console.log('tower1', tower1)
		console.log('tower2', tower2)
		console.log('tower3', tower3)
		console.log()
		// console.log('source', this.index, this)
		// console.log('buffer', buffer.index, buffer)
		// console.log('destin', destination.index, destination)
		// console.log()
		console.log('switching source and buffer')
		console.log('source', this.index, this)
		console.log('buffer', buffer.index, buffer)
		buffer.moveDisks(N-1, destination, this, tower1, tower2, tower3)
	}
};

Tower.prototype.moveTopTo = function(tower) {
	var top = this.stack.pop()
	tower.stack.push(top)
};

towers(1)