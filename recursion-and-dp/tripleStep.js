// Cracking The Coding Interview
// Recursion and Dynamic Programming
// 8.1

// Not posting problem description for copyright concerns.
// Please see Cracking The Coding Interview, 6th edition
// for full problem description.

/**
	if(memo === undefined) seemed like it might affect perf
	with repeated checks that don't matter after the first
	iteration. Benchmarked with and without. Steps was set
	to 1100. Approx. max term where the number of paths is
	less than JS "Infinity".

	Trial 1 (with if statement): 22ms - 25ms
	Trial 2 (without): 22ms - 25ms

	No meaureable difference. Going with this method because
	of simpler API.
**/

function tripleStep(steps, memo){
	// just so function can be called as tripleStep(steps)
	if(memo === undefined){
		memo = {};
	}

	if(memo.hasOwnProperty(steps)){
		return memo[steps]
	}
	if(steps < 0){
		var paths = 0;
	}
	else if(steps === 0){
		var paths = 1;
	}
	else{
		var paths = tripleStep(steps-1, memo) + tripleStep(steps-2, memo) + tripleStep(steps-3, memo);
	}
	
	memo[steps] = paths;
	console.log(memo[steps])
	return paths
}

tripleStep(6)
