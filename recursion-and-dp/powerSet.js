// Return all subsets of a set
// Time Complexity:
/*
	
*/
function powerSet(arr, masterSet, start, runner){
	if(start === arr.length){
		console.log('finished')
		return masterSet
	}
	if(runner === arr.length+1){
		return powerSet(arr, masterSet, start+1, start+2)
	}
	masterSet.push(arr.slice(start, runner))
	return powerSet(arr, masterSet, start, runner+1)
}

function getPowerSet(arr){
	var subsets = powerSet(arr, [], 0, 1)
	console.log('printing subsets', subsets)
}

var set = [1,2,3,4,5]
getPowerSet(set)