// Cracking The Coding Interview
// Recursion and Dynamic Programming
// 8.5

// Not posting problem description for copyright concerns.
// Please see Cracking The Coding Interview, 6th edition
// for full problem description.

// no * operator allowed.
function recursiveMult(a, b){
	if(a < 0){
		a = -a;
		b = -b;
	}

	if(a === 0 || b === 0){
		return 0
	}

	// base case
	if(a === 1){
		return b
	}

	return b + recursiveMult(a-1, b);
}

console.log(recursiveMult(1,2))
console.log(recursiveMult(2,2))
console.log(recursiveMult(3,4))
console.log(recursiveMult(5,10))

console.log(recursiveMult(-1,2))
console.log(recursiveMult(-2,2))
console.log(recursiveMult(-3,4))
console.log(recursiveMult(-5,10))

console.log(recursiveMult(1,-2))
console.log(recursiveMult(2,-2))
console.log(recursiveMult(3,-4))
console.log(recursiveMult(5,-10))

console.log(recursiveMult(-1,-2))
console.log(recursiveMult(-2,-2))
console.log(recursiveMult(-3,-4))
console.log(recursiveMult(-5,-10))

console.log(recursiveMult(0,2))
console.log(recursiveMult(0,2))
console.log(recursiveMult(0,4))
console.log(recursiveMult(0,10))