(function(){
	/*
		Replace spaces with %20
	*/
	module.exports = {
						urlify: urlify
					 }
	function urlify(str){
		var strBuild = []
		for(var i = 0; i < str.length; i++){
			if(str[i] === ' '){
				strBuild[i] = '%20'
			}
			else{
				strBuild[i] = str[i]
			}
		}

		var urlStr = strBuild.join('')
		return urlStr
	}
})()