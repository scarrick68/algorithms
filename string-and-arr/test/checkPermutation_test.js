var permutation = require('../checkPermutation')
var expect = require('chai').expect

var perm1 = 'light'
var perm2 = 'gliht'

var permDups1 = 'liigght'
var permDups2 = 'ggliiht'

var notPerm1 = 'halloween'
var notPerm2 = 'hlloween'

var empty = ''
describe('Determine if two strings are permutations of each other.', function(){
    describe('Determine if two strings are permutations of each other.', function(){
        it('They are permutations', function(){
            var test1 = permutation.checkPermutation(perm1, perm2)
            expect(test1).to.equal(true)
        });

        it('They are permutations and have duplicate letters.', function(){
            var test1 = permutation.checkPermutation(permDups1, permDups2)
            expect(test1).to.equal(true)
        });

        it('They are not permutations.', function(){
            var test1 = permutation.checkPermutation(notPerm1, notPerm2)
            expect(test1).to.equal(false)
        });    

        it('Testing with empty strings.', function(){
            var test1 = permutation.checkPermutation(empty, empty)
            expect(test1).to.equal(true)
        });

        it('Testing with one empty and one non-empty string.', function(){
            var test1 = permutation.checkPermutation(empty, perm1)
            expect(test1).to.equal(false)
        });
    });
});