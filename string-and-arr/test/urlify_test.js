var urlify = require('../urlify')
var expect = require('chai').expect

var justSpace = ' '
var noSpace = 'Hello'
var empty = ''
var hasSpace = 'Hello World'
var multiSpace = 'Hello World And Stuff'

describe('Replace spaces with %20', function(){
    it('Only a space', function(){            
        var test1 = urlify.urlify(justSpace)
        expect(test1).to.equal('%20')
    });

    it('No space', function(){            
        var test1 = urlify.urlify(noSpace)
        expect(test1).to.equal('Hello')
    });

    it('Empty str', function(){            
        var test1 = urlify.urlify(empty)
        expect(test1).to.equal('')
    });

    it('Has one space', function(){            
        var test1 = urlify.urlify(hasSpace)
        expect(test1).to.equal('Hello%20World')
    });

    it('Has many spaces', function(){            
        var test1 = urlify.urlify(multiSpace)
        expect(test1).to.equal('Hello%20World%20And%20Stuff')
    });
});