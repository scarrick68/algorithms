var palindromePerm = require('../palindromePermutation')
var expect = require('chai').expect

var str1 = 'tacocat'
var str2 = 'battab'
var str3 = 'abba'
var str4 = 'acotcat'
var oneLetter = 'a'
var empty = ''
var notPalindrome = 'abca'
var spaces1 = 'taco cat'		// note the space
var spaces2 = 'tac o cat'

describe('Check if a there exists a permutation that makes a palindrome', function(){
    it('already palindromes', function(){            
        var test1 = palindromePerm.possiblePalindrome(str1)
        var test2 = palindromePerm.possiblePalindrome(str2)
        var test3 = palindromePerm.possiblePalindrome(str3)
        
        expect(test1).to.equal(true)
        expect(test2).to.equal(true)
        expect(test3).to.equal(true)
    });

    it('single character', function(){
        var test1 = palindromePerm.possiblePalindrome(oneLetter)
        expect(test1).to.equal(true)
    });

    it('empty str', function(){
        var test1 = palindromePerm.possiblePalindrome(empty)
        expect(test1).to.equal(true)
    });

    it('not palindrome', function(){
        var test1 = palindromePerm.possiblePalindrome(notPalindrome)
        expect(test1).to.equal(false)
    });

    it('spaces count', function(){
        var test1 = palindromePerm.possiblePalindrome(spaces1)
        var test2 = palindromePerm.possiblePalindrome(spaces2)

        expect(test1).to.equal(false)
        expect(test2).to.equal(true)
    });
});