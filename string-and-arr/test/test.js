var uniqueChars = require('../isUnique')
var expect = require('chai').expect

var uniqueStr = 'light'
var notUniqueStr = 'halloween'
var uniqueStrCaps = 'Light'
var notUniqueStrCaps = 'haLlowEen'
var empty = ''

describe('Determine if characters in a string are unique', function(){
    describe('With Additional Data Structure', function(){
        it('Returns true if all chars are unqiue. Else, false.', function(){            
            var test1 = uniqueChars.isUnique(uniqueStr)
            var test2 = uniqueChars.isUnique(notUniqueStr)
            expect(test1).to.equal(true)
            expect(test2).to.equal(false)
        });

        it('Testing with capital letters.', function(){            
            var test1 = uniqueChars.isUnique(uniqueStrCaps)
            var test2 = uniqueChars.isUnique(notUniqueStrCaps)
            expect(test1).to.equal(true)
            expect(test2).to.equal(false)
        });

		it('Testing with empty string', function(){            
            var test1 = uniqueChars.isUnique(empty)
            expect(test1).to.equal(true)
        });
    });

    describe('Without Additional Data Structure', function(){
        it('Returns true if all chars are unqiue. Else, false.', function(){
        	var test1 = uniqueChars.isUniqueNDS(uniqueStr)
            var test2 = uniqueChars.isUniqueNDS(notUniqueStr)
            expect(test1).to.equal(true)
            expect(test2).to.equal(false)
        })

        it('Testing with capital letters.', function(){
            var test1 = uniqueChars.isUniqueNDS(uniqueStrCaps)
            var test2 = uniqueChars.isUniqueNDS(notUniqueStrCaps)
            expect(test1).to.equal(true)
            expect(test2).to.equal(false)
        });

		it('Testing with empty string', function(){
		    var test1 = uniqueChars.isUnique(empty)
		    expect(test1).to.equal(true)
        });
    });
});