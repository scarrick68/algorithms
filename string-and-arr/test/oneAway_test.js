var oneAway = require('../oneAway')
var expect = require('chai').expect

var str1 = 'tacocat'
var str2 = 'tacocab'

var str15 = 'tacocat'
var str16 = 'tacoct'

var str3 = ''
var str4 = 'a'

var str5 = 'a'
var str6 = ''

var str7 = 'a'
var str8 = 'b'

var str9 = 'a'
var str10 = 'a'

var str11 = 'hello'
var str12 = 'helloo'

var str13 = 'hello'
var str14 = 'hellob'

var str17 = 'hello'
var str18 = 'hallob'

var str19 = 'a'
var str20 = 'bac'
describe('Check if a string is one edit away from another', function(){
    it('One replacement', function(){            
        var test1 = oneAway.oneAway(str1, str2)
        expect(test1).to.equal(true)

        var test2 = oneAway.oneAway(str7, str8)
        expect(test1).to.equal(true)
    });

    it('One delete', function(){            
        var test1 = oneAway.oneAway(str15, str16)
        expect(test1).to.equal(true)
    });

    it('One insert. Existing char and new char.', function(){            
        var test1 = oneAway.oneAway(str11, str12)
        expect(test1).to.equal(true)

        var test1 = oneAway.oneAway(str13, str14)
        expect(test1).to.equal(true)
    });    

    it('single character and empty str. insert and delete.', function(){
        var test1 = oneAway.oneAway(str3, str4)
        expect(test1).to.equal(true)

        var test2 = oneAway.oneAway(str5, str6)
        expect(test2).to.equal(true)
    });

    it('multiple edits', function(){
        var test1 = oneAway.oneAway(str17, str18)
        expect(test1).to.equal(false)

        var test2 = oneAway.oneAway(str19, str20)
        expect(test2).to.equal(false)
    });

    it('no edits / replace with same character', function(){
        var test1 = oneAway.oneAway(str9, str10)
        expect(test1).to.equal(true)
    });
});