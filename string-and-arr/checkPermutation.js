(function(){
	/*
		Check if one string is a permutation of the other.
		Gonna treat upper and lower case as diff letters.
		Easy to change and this decision is arbitrary.
	*/

	module.exports = {
						checkPermutation: checkPermutation
					 }

	// handles strings with duplicate characters
	function checkPermutation(str1, str2){
		var obj = arrToObj(str1)
		if(str1.length !== str2.length){
			return false
		}

		for(var i = 0; i<str2.length; i++){
			var currVal = str2[i]
			if(obj.hasOwnProperty(currVal)){
				obj[currVal] = obj[currVal] - 1
			}
			else if(!obj.hasOwnProperty(currVal)){
				return false
			}

			if(obj[currVal] === 0){
				delete obj[currVal]
				obj.length = obj.length - 1
			}
		}

		if(obj.length === 0){
			return true
		}

		return false
	}

	// array or str. w/e.
	function arrToObj(arr){
		var obj = {}
		obj.length = 0
		for(var i = 0; i<arr.length; i++){
			var currVal = arr[i]
			if(obj.hasOwnProperty(currVal)){
				obj[currVal]++
			}
			if(!obj.hasOwnProperty(currVal)){
				obj[currVal] = 1
				obj.length++
			}
		}

		return obj
	}
})()