(function(){
	/*
		Are all chars in a str unique?
		What if you can't use additional data structures
	*/
	module.exports = {
						isUnique: isUnique,
						isUniqueNDS: isUniqueNDS
					 }

	function isUnique(str){
		var obj = {}
		obj.length = 0
		for(var i = 0; i < str.length; i++){
			if(!obj.hasOwnProperty(str[i])){
				var character = str[i].toLowerCase()
				obj[character] = true
				obj.length++
			}
		}

		if(obj.length !== str.length){
			return false
		}

		return true
	}

	// stands for is unique no data structure
	function isUniqueNDS(str){
		for(var i = 0; i<str.length; i++){
			var currentChar = str[i].toLowerCase()
			for(var j = i+1; j<str.length; j++){
				var compareChar = str[j].toLowerCase()
				if(currentChar === compareChar){
					return false
				}
			}
		}

		return true
	}
})()