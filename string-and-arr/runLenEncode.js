(function(){
	/*
		run length encoding.
	*/

	function rleStrComp(str){
		if(str.length === 0){
			return ''
		}
		var count = 1
		var prevVal = str[0]
		var accumulator = []
		for(var i = 1; i<str.length; i++){
			var currVal = str[i]
			if(currVal === prevVal){
				count++
			}
			else{
				accumulator.push(prevVal+count)
				count = 0
			}
		}

		var compressedStr = accumulator.join('')
		return compressedStr
	}
})()