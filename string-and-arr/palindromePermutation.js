(function(){
	/*
		Determine if the input is a permutation of a palindrome.
		We just need to see if a palindrome is possible from this
		set of characters. Spaces and punctuation are counting
		as characters.
	*/

	module.exports = {
		possiblePalindrome: possiblePalindrome
	}
	function possiblePalindrome(str){
		var letterCount = countOccur(str)
		var oddCount = 0
		for(var i in letterCount){
			if(letterCount[i] % 2 !== 0){
				oddCount++
				if(oddCount > 1){
					return false
				}
			}
		}

		return true
	}

	// count occurences of elements in str or arr
	function countOccur(arr){
		var obj = {}
		for(var i = 0; i < arr.length; i++){
			var currVal = arr[i]
			if(!obj.hasOwnProperty(currVal)){
				obj[currVal] = 1
			}
			else{
				obj[currVal]++
			}
		}
		
		return obj
	}
})()