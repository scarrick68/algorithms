(function(){
	/*
		Return true if two strings are at most one edit
		away, else false. Edits are insert, replace, remove.

		Throw one str into an object, iterate over the second
		and compare existence of characters, keep count of
		inconsistencies (the edits). Need to loop over the
		longer of the two in case of an insert.
	*/
	module.exports = {
		oneAway: oneAway
	}
	function oneAway(str1, str2){
		var longerStr = ''
		var shorterStr = ''
		if(str1.length >= str2.length){
			longerStr = str1
			shorterStr = str2
		}
		else{
			longerStr = str2
			shorterStr = str1
		}

		var letterCount = arrToObj(shorterStr)
		var edits = 0
		for(var i = 0; i<longerStr.length; i++){
			var currVal = longerStr[i]
			if(!letterCount.hasOwnProperty(currVal)){
				edits++
				if(edits > 1){
					return false
				}
			}
			else if(letterCount.hasOwnProperty(currVal)){
				letterCount[currVal]--
			}
			if(letterCount[currVal] === 0){
				delete letterCount[currVal]
			}
		}

		return true

	}

	function arrToObj(arr){
		var obj = {}
		for(var i = 0; i < arr.length; i++){
			var currVal = arr[i]
			if(obj.hasOwnProperty(currVal)){
				obj[currVal]++
			}
			else{
				obj[currVal] = 1
			}
		}

		return obj
	}
})()